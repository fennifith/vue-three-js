import { Selection } from "postprocessing";
import { InjectionKey, Ref } from "vue";
import { ThreeConfig, ThreeContext } from "./types";

export const ThreeConfigSymbol: InjectionKey<Ref<ThreeConfig>> = Symbol("THREEJS_CONFIG");

export const ThreeContextSymbol: InjectionKey<Ref<ThreeContext>> = Symbol("THREEJS_CONTEXT");

export const BloomSelectionSymbol: InjectionKey<Selection> = Symbol("THREEJS_BLOOM_SELECTION");

export const EventTargetSymbol: InjectionKey<EventTarget> = Symbol("THREEJS_EVENT_TARGET");
