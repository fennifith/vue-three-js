import { EffectComposer } from "postprocessing";
import { Camera, Material, Object3D, Scene, WebGLRenderer } from "three";
import { Ref } from "vue";

export type ThreeConfig = {
	renderer: Ref<WebGLRenderer|null>,
	composer: Ref<EffectComposer|null>,
};

export type ThreeContext = {
	canvasElement: Ref<HTMLCanvasElement|null>,
	camera: Camera,
	scene: Scene,
	object: Object3D,
	material: Material,
	isRunning: boolean;
};

/**
 * <Rotation x="90">
 * 	<Box>
 * </Rotation>
 */