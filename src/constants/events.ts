import { EventKey } from "../utils/events";

export const BEFORE_RENDER: EventKey<number> = "before-render";
export const AFTER_RENDER: EventKey<number> = "after-render";
export const RENDER_START: EventKey<void> = "render-start";
export const RENDER_STOP: EventKey<void> = "render-stop";
