export type TimeUnit = number|string;

export const fps = 60;

const units: Record<string, (v: number) => number> = {
	ms: v => v * fps / 1000,
	s: v => v * fps,
	m: v => v * 60 * fps,
	f: v => v,
};

export function unitsToFrames(value: TimeUnit) {
	if (typeof value === "number") return value;

	let unit = /[a-z]+$/.exec("" + value)?.[0];
	let number = +("" + value).slice(0, -(unit?.length || 0));
	unit ||= "s";

	if (!units[unit]) throw new Error(`Invalid time unit for time value '${number}' '${unit}' - available units are ${Object.keys(units)}`);

	return Math.floor(units[unit](number));
}
