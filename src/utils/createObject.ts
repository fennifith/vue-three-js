import { Object3D } from "three";
import { defineComponent } from "vue";

export type ObjectProps = {
	object: Object3D,
};

export function createObject(props: ObjectProps) {
	// TODO: decide on a function-based version of <ThreeObject>
}
