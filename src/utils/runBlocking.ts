let promises: Promise<unknown>[] = (window as any).vue3js_blocking_promises ?? [];
(window as any).vue3js_blocking_promises = promises;

export function runBlocking(callback: unknown) {
	if (callback instanceof Promise) {
		promises.push(callback);
		return;
	}

	if (typeof callback === "function") {
		const ret = callback();
		if (ret instanceof Promise)
			promises.push(ret);
	}
};

export async function waitBlocking() : Promise<void> {
	while (promises.length > 0) {
		const p = promises.pop();
		await p;
	}
}
