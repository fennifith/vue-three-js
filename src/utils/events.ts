import { inject, onScopeDispose, provide } from "vue";
import { EventTargetSymbol } from "../constants/symbols";
import { runBlocking } from "./runBlocking";

export interface EventKey<T> extends String {
	__type?: T,
}

export function useEventTarget(): EventTarget {
	return window;
	/*
	let eventTarget = inject(EventTargetSymbol, null);

	if (eventTarget === null) {
		console.log("Creating new Event Target");
		eventTarget = new EventTarget();
		provide(EventTargetSymbol, eventTarget);
	}

	return eventTarget;
	*/
}

export function createEvent<T>(key: EventKey<T>) {
	function useEvent() {
		const eventTarget = useEventTarget();

		return {
			emit(value: T) {
				eventTarget.dispatchEvent(new CustomEvent(key.toString(), {
					detail: value,
				}));
			}
		};
	}

	function onEvent(callback: (value: T) => any) {
		const eventTarget = useEventTarget();

		const handleEvent = (e: Event) => runBlocking(callback((e as CustomEvent).detail));
		eventTarget.addEventListener(key.toString(), handleEvent);

		onScopeDispose(() => {
			eventTarget.removeEventListener(key.toString(), handleEvent);
		});
	}

	return { useEvent, onEvent };
}

