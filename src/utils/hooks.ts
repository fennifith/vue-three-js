import { Object3D } from "three";
import { inject, isRef, onMounted, onUnmounted, provide, Ref, shallowRef, unref, watch, watchEffect } from "vue";
import { AFTER_RENDER, BEFORE_RENDER, RENDER_START, RENDER_STOP } from "../constants/events";
import { ThreeConfigSymbol, ThreeContextSymbol } from "../constants/symbols";
import { ThreeContext } from "../constants/types";
import { createEvent } from "./events";

export function injectConfig() {
	const config = inject(ThreeConfigSymbol, null);
	if (!config) {
		throw "injectConfig: three.js configuration is null! Are you sure this is inside a <Canvas> component?";
	}

	return config;
}

export function injectContext() {
	const context = inject(ThreeContextSymbol, null);
	if (!context) {
		throw "injectContext: three.js configuration is null! Are you sure this is inside a <Canvas> component?";
	}

	return context;
}

export const { useEvent: useBeforeRender, onEvent: onBeforeRender } = createEvent(BEFORE_RENDER);
export const { useEvent: useAfterRender, onEvent: onAfterRender } = createEvent(AFTER_RENDER);
export const { useEvent: useRenderStart, onEvent: onRenderStart } = createEvent(RENDER_START);
export const { useEvent: useRenderStop, onEvent: onRenderStop } = createEvent(RENDER_STOP);

export function useThreeObject<T extends undefined|Object3D>(mesh: T | Ref<T>) {
	const ctx = injectContext();
	let currentMesh: T;

	const cleanup = () => {
		if (currentMesh) {
			ctx.value.object.remove(currentMesh);
			console.log(`RUNNING CLEANUP FOR ${currentMesh.uuid}`);
		}
	}

	onMounted(() => {
		cleanup();

		currentMesh = unref(mesh);

		if (currentMesh) {
			ctx.value.object.add(currentMesh);
		}
	});

	if (isRef(mesh)) {
		watch(mesh, () => {
			cleanup();
			currentMesh = unref(mesh);

			if (currentMesh) {
				ctx.value.object.add(currentMesh);
			}
		});
	}

	onUnmounted(cleanup);
}

export function useContextTransform(transform: (ctx: ThreeContext) => ThreeContext) {
	const context = injectContext();
	const contextTransformed = shallowRef(context.value);
	watchEffect(() => {
		contextTransformed.value = transform(context.value);
	})
	provide(ThreeContextSymbol, contextTransformed);
}
