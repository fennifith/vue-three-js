import { useAsyncState } from "@vueuse/core";
import { isRef, ref, Ref } from "vue";

export function toRef<T>(arg: T | (() => T) | Promise<T> | Ref<T>) : Ref<T|null> {
	if (isRef(arg))
		return arg;
	if (arg instanceof Promise)
		return useAsyncState(arg, null).state;
	if (arg instanceof Function)
		return toRef(arg());

	return ref(arg) as Ref<T|null>;
}
