import { Vector3, Quaternion } from "three";

export type Vector3Like = [number, number, number] | {x: number, y: number, z: number} | Vector3;

export function toVector3(v: Vector3Like): Vector3 {
	if (v instanceof Vector3)
		return v;
	if (v instanceof Array)
		return new Vector3(v[0], v[1], v[2]);

	return new Vector3(v.x, v.y, v.z);
}

export function toAxisAngle(axis: Vector3Like, angle: number) {
	return new Quaternion().setFromAxisAngle(toVector3(axis), angle);
}
