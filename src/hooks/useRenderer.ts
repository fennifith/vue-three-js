import { useWindowSize } from "@vueuse/core";
import { BlendFunction, DepthDownsamplingPass, EdgeDetectionMode, EffectComposer, EffectPass, GodRaysEffect, NormalPass, PredicationMode, RenderPass, SMAAEffect, SMAAPreset, FXAAEffect, SSAOEffect, ToneMappingEffect, ToneMappingMode, SelectiveBloomEffect, BloomEffect, Selection, KawaseBlurPass, TextureEffect, BlendMode, DepthOfFieldEffect, KernelSize } from "postprocessing";
import { Camera, FogExp2, FogBase, HalfFloatType, Matrix4, MeshBasicMaterial, PerspectiveCamera, Scene, Vector3, VSMShadowMap, WebGLRenderer, PointLight, SphereGeometry, Mesh } from "three";
import { computed, inject, onScopeDispose, provide, reactive, readonly, Ref, shallowRef, triggerRef, watch, watchEffect } from "vue";
import { AFTER_RENDER, BEFORE_RENDER } from "../constants/events";
import { BloomSelectionSymbol, ThreeConfigSymbol, ThreeContextSymbol } from "../constants/symbols";
import { ThreeConfig, ThreeContext } from "../constants/types";
import { onRenderStart, onRenderStop, useAfterRender, useBeforeRender } from "../utils/hooks";
import { currentFrame, useRenderLoop } from "./useRenderLoop";

export type RendererOpts = {
	scene: Scene;
	renderer: Ref<WebGLRenderer|null>;
	composer: Ref<EffectComposer|null>;
	camera: Camera;
	size: { width: number, height: number };
	canvasElement: Ref<HTMLCanvasElement|null>;
	clearColor: number;
	fog: FogBase;
	bloomSelection: Selection;
	autoplay: boolean;
	isHighQuality: boolean;
};

export function createEffects(opts: Omit<RendererOpts, "composer" | "clearColor" | "fog" | "autoplay">) {
	const composer = new EffectComposer(opts.renderer.value!, {
		frameBufferType: HalfFloatType,
	});
	composer.setSize(opts.size.width, opts.size.height, false);

	// const normalPass = new NormalPass(opts.scene, opts.camera);
	// const depthDownsamplingPass = new DepthDownsamplingPass({
	// 	normalBuffer: normalPass.texture,
	// 	resolutionScale: 0.5,
	// });

	const smaaEffect = new SMAAEffect({
		preset: opts.isHighQuality ? SMAAPreset.ULTRA : SMAAPreset.HIGH,
		edgeDetectionMode: EdgeDetectionMode.COLOR,
		predicationMode: PredicationMode.DEPTH,
	});

	smaaEffect.edgeDetectionMaterial.edgeDetectionThreshold = 0.02;
	smaaEffect.edgeDetectionMaterial.predicationThreshold = 0.002;
	smaaEffect.edgeDetectionMaterial.predicationScale = 1;

	const tonemappingEffect = new ToneMappingEffect({
		mode: ToneMappingMode.ACES_FILMIC,
		resolution: 256,
		whitePoint: 16.0,
		middleGrey: 0.6,
		minLuminance: 0.01,
		averageLuminance: 0.01,
		adaptationRate: 1.0,
		blendFunction: BlendFunction.AVERAGE,
	});

	const selectiveBloomEffect = new SelectiveBloomEffect(opts.scene, opts.camera, {
		blendFunction: BlendFunction.ADD,
		mipmapBlur: true,
		luminanceThreshold: 0,
		luminanceSmoothing: 0.3,
		intensity: 2.0,
	});
	selectiveBloomEffect.selection = opts.bloomSelection;

	const bloomEffect = new BloomEffect({
		mipmapBlur: true,
		luminanceThreshold: 0.2,
		luminanceSmoothing: 0.3,
		intensity: 2,
	});

	composer.addPass(new RenderPass(opts.scene, opts.camera));
	// composer.addPass(normalPass);
	// composer.addPass(depthDownsamplingPass);

	composer.addPass(new EffectPass(opts.camera, smaaEffect, selectiveBloomEffect, tonemappingEffect));
	composer.multisampling = opts.isHighQuality ? 8 : 0;

	return composer;
}

export function useThreeContext({
	canvasElement = shallowRef(null),
	scene = new Scene(),
	camera = (() => {
		const camera = new PerspectiveCamera(45, 1, 1, 500);
		camera.position.set(0, 0, 100);
		camera.lookAt(new Vector3(0, 0, 0));
		return camera;
	})(),
	material = new MeshBasicMaterial({ color: 0x00ff00 }),
	isRunning = true,
}: Partial<ThreeContext> = {}) {
	const ctx = inject(ThreeContextSymbol, null);
	if (ctx) return ctx;

	const context: Ref<ThreeContext> = shallowRef({
		canvasElement,
		scene,
		object: scene,
		camera,
		material,
		isRunning,
	});

	provide(ThreeContextSymbol, context);
	return context;
}

export function useRenderer({
	size = reactive(useWindowSize()),
	renderer = shallowRef(null),
	composer = shallowRef(null),
	bloomSelection = new Selection(),
	autoplay = true,
	isHighQuality = false,
	...opts
}: Partial<RendererOpts>) {
	const context: Ref<ThreeContext> = useThreeContext({
		canvasElement: opts.canvasElement,
		scene: opts.scene,
		camera: opts.camera,
		isRunning: autoplay,
	});

	const isReady = computed(() => !!context.value.canvasElement.value && !!renderer.value);

	const afterRender = useAfterRender();
	const beforeRender = useBeforeRender();

	useRenderLoop(() => {
		if (!context.value.isRunning || !isReady.value) return;

		// draw the previous frame when ready
		composer.value?.render();
		afterRender.emit(currentFrame.value);

		// emit BEFORE_RENDER before the next frame...
		beforeRender.emit(currentFrame.value);
	});

	// runs if the provided `canvasElement` ref updates, to construct the default renderer
	const shouldCreateRenderer = !renderer.value && !composer.value;
	watch(context.value.canvasElement, (canvas, prevCanvas) => {
		if (!shouldCreateRenderer || !canvas || !!prevCanvas)
			return;

		renderer.value?.dispose();
		composer.value?.dispose();

		renderer.value = new WebGLRenderer({
			canvas,
			powerPreference: isHighQuality ? "low-power" : "high-performance",
			antialias: false,
			stencil: false,
			depth: false,
			preserveDrawingBuffer: true,
		});
		renderer.value.setSize(size.width, size.height, false);
		renderer.value.shadowMap.enabled = true;
		renderer.value.shadowMap.type = VSMShadowMap;

		if (isHighQuality)
			renderer.value.setPixelRatio(2);

		context.value.canvasElement.value = renderer.value.domElement;

		composer.value = createEffects({
			scene: context.value.scene,
			camera: context.value.camera,
			canvasElement: context.value.canvasElement,
			renderer,
			size,
			bloomSelection,
			isHighQuality,
		});
	});

	// runs if the window/canvas is resized
	watchEffect(() => {
		const { width, height } = size;
		renderer.value?.setSize(width, height, false);
		composer.value?.setSize(width, height, false);

		const { camera } = context.value;
		if ("aspect" in camera && "updateProjectionMatrix" in camera && typeof camera.updateProjectionMatrix === "function") {
			camera.aspect = width / height;
			camera.updateProjectionMatrix();
		}
	});

	// runs if the renderer is recreated, or if opts.clearColor is shallowReactive
	watchEffect(() => {
		const clearColor = opts.clearColor || 0;
		renderer.value?.setClearColor(clearColor);
		context.value.scene.fog ||= opts.fog || new FogExp2(clearColor, 0.005);
	});

	const config: Ref<ThreeConfig> = shallowRef({
		renderer,
		composer,
	});

	provide(ThreeConfigSymbol, config);
	provide(BloomSelectionSymbol, bloomSelection);

	// clean up existing renderer/composer before unmount
	onScopeDispose(() => {
		renderer.value?.setAnimationLoop(null);
		renderer.value?.dispose();
		composer.value?.dispose();
	});

	onRenderStart(() => {
		context.value.isRunning = true;
		triggerRef(context);
	});

	onRenderStop(() => {
		context.value.isRunning = false;
		triggerRef(context);
	});

	return {
		renderer, composer, config, context, isReady,
	};
}
