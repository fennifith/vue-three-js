import { AnimationAction, AnimationClip, AnimationMixer, Clock, Object3D } from "three";
import { GLTF, GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { computed, Ref, unref, UnwrapRef } from "vue";
import { onBeforeRender, useThreeObject } from "../utils/hooks";

const loader = new GLTFLoader();

/**
 * Loads the GLTF model data from its file path and set up its config
 */
export async function createGLTFModel(modelPath: string) : Promise<GLTF> {
	const model = await loader.loadAsync(modelPath);

	function setReceiveShadow(obj: Object3D) {
		obj.receiveShadow = true;
		obj.castShadow = true;
		obj.children.forEach(setReceiveShadow);
	}

	setReceiveShadow(model.scene);

	return model;
}

/**
 * Given a loaded GLTF model or ref, uses it in the current ThreeJS context.
 */
export function useGLTFModel<T extends GLTF | undefined>(model: T | Ref<T>) {
	const scene = computed(() => unref(model)?.scene);
	useThreeObject(scene);
}

type AnimationConfig = {
	loop?: Parameters<AnimationAction["setLoop"]>;
	duration?: number;
	effectiveTimeScale?: number;
	effectiveWeight?: number;
};

/**
 * Construct an animator utility for the given keys that can manage
 * animations defined in the GLTF model.
 */
export function useGLTFAnimator<T extends GLTF | undefined, K extends string>(
	model: T | Ref<T>,
	animations: Record<K, AnimationConfig>
) {
	const mixer = computed(() => {
		const scene = unref(model)?.scene;
		return scene && new AnimationMixer(scene);
	});

	const clips = computed(() => {
		const m = unref(model);
		const mix = unref(mixer);
		if (!m || !mix) return {};

		const entries = Object.entries(animations).map(([key, configUnknown]) => {
			const config = configUnknown as AnimationConfig;
			let clip = mix.clipAction(AnimationClip.findByName(m.animations, key));
			if (config.loop) clip = clip.setLoop(...config.loop);
			if (config.duration) clip = clip.setDuration(config.duration);
			if (config.effectiveTimeScale) clip = clip.setEffectiveTimeScale(config.effectiveTimeScale);
			if (config.effectiveWeight) clip = clip.setEffectiveWeight(config.effectiveWeight);
			return [key, clip];
		});

		return Object.fromEntries(entries) as Record<string, AnimationAction>;
	});

	onBeforeRender(() => {
		unref(mixer)?.update(1/60);
	});

	function animate(key: K) {
		clips.value[key].reset();
		return clips.value[key].play();
	}

	return { clips, animate };
}
