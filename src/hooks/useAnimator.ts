import { Ref, ref } from 'vue';
import { onBeforeRender } from '../utils/hooks';
import { unitsToFrames } from '../utils/units';
import { Euler, Quaternion, Vector2, Vector3, Vector4 } from 'three';

const animatorFns = {
	linear: (x: number) => x,
	//easeIn: (x: number) => Math.pow(x, 2),
	easeIn: (x: number) => 1 - Math.cos((x * Math.PI) / 2),
	//easeOut: (x: number) => Math.sqrt(x),
	easeOut: (x: number) => Math.sin((x * Math.PI) / 2),
	//easeInOut: (x: number) => Math.pow(2*x - 1, 1/3) / 2,
	easeInOut: (x: number) => -(Math.cos(Math.PI * x) - 1) / 2,
} as const;

export type AnimatorPoint<T> = {
	type?: keyof typeof animatorFns,
	value: T;
	duration: number|string;
};

type DefinedAnimatorPoint<T> = AnimatorPoint<T> & {
	startValue: T;
	start: number;
	end: number;
};

export type ScalingFn<T> = (from: T, to: T, scale: number) => T;

export function useBaseAnimator<T>(
	initialValue: T,
	scalingFn: ScalingFn<T>,
	callback?: (value: T) => void,
) {
	let frame = 0;
	let animatedPoints: DefinedAnimatorPoint<T>[] = [];
	const value = ref(initialValue) as Ref<T>;

	function push(...points: AnimatorPoint<T>[]) {
		// assign all frames to a specific start frame + duration
		const lastPoint = animatedPoints.at(-1);

		let lastFrame = lastPoint ? lastPoint.start + unitsToFrames(lastPoint.duration) : frame;
		let lastValue = lastPoint ? lastPoint.value : value.value;
		for (const point of points) {
			animatedPoints.push({
				...point,
				startValue: lastValue,
				start: lastFrame,
				end: lastFrame + unitsToFrames(point.duration),
			});

			lastValue = point.value;
			lastFrame += unitsToFrames(point.duration);
		}
	}

	function to(...points: AnimatorPoint<T>[]) {
		animatedPoints.length = 0;
		push(...points);
	}

	function clear() {
		animatedPoints.length = 0;
	}

	onBeforeRender((currentFrame) => {
		frame = currentFrame;

		const currentPoint = animatedPoints.find(p => p.start <= frame && p.end > frame);
		if (!currentPoint) {
			if (animatedPoints.length) {
				const point = animatedPoints.at(-1)!!;
				value.value = point.value;
				callback?.call(undefined, value.value);
				animatedPoints.length = 0;
			}

			return;
		}

		const framesPassed = frame - currentPoint.start;
		const framesTotal = unitsToFrames(currentPoint.duration);

		const ratio = Math.max(0, Math.min(1, framesPassed / framesTotal));
		const progress = animatorFns[currentPoint.type || "linear"](ratio);
		value.value = scalingFn(currentPoint.startValue, currentPoint.value, progress);
		callback?.call(undefined, value.value);
	});

	return {
		to,
		push,
		clear,
		value,
		get isAnimating() {
			const currentPoint = animatedPoints.find(p => p.start <= frame && p.end >= frame);
			return !!currentPoint;
		},
	};
}

export function useAnimator(
	initialValue: number,
	callback?: (value: number) => void,
) {
	return useBaseAnimator<number>(
		initialValue,
		(from, to, scale) => from + (to - from) * scale,
		callback,
	);
}

export function useVec2Animator(
	initialValue: Vector2,
	callback?: (value: Vector2) => void,
) {
	return useBaseAnimator<Vector2>(
		initialValue,
		(from, to, scale) => from.clone().lerp(to, scale),
		callback,
	);
}

export function useVec3Animator(
	initialValue: Vector3,
	callback?: (value: Vector3) => void,
) {
	return useBaseAnimator<Vector3>(
		initialValue,
		(from, to, scale) => from.clone().lerp(to, scale),
		callback,
	);
}

export function useVec4Animator(
	initialValue: Vector4,
	callback?: (value: Vector4) => void,
) {
	return useBaseAnimator<Vector4>(
		initialValue,
		(from, to, scale) => from.clone().lerp(to, scale),
		callback,
	);
}

export function useQuatAnimator(
	initialValue: Quaternion,
	callback?: (value: Quaternion) => void,
) {
	return useBaseAnimator<Quaternion>(
		initialValue,
		(from, to, scale) => from.clone().slerp(to, scale),
		callback,
	);
}

export function useEulerAnimator(
	initialValue: Euler,
	callback?: (value: Euler) => void,
) {
	return useBaseAnimator<Euler>(
		initialValue,
		(from, to, scale) => new Euler().setFromQuaternion(
			new Quaternion().setFromEuler(from)
				.slerp(new Quaternion().setFromEuler(to), scale)
		),
		callback,
	);
}
