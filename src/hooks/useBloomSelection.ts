import { Object3D } from "three";
import { inject, isRef, onMounted, onScopeDispose, Ref, unref, watch } from "vue";
import { BloomSelectionSymbol } from "../constants/symbols";

export function useBloomSelection<T extends undefined|Object3D>(mesh: T | Ref<T>) {
	const bloomSelection = inject(BloomSelectionSymbol);
	if (!bloomSelection) throw "No bloom selection available - are you sure this is inside a <Canvas> component?";

	let currentMesh: T;

	const cleanup = () => {
		if (currentMesh) bloomSelection.delete(currentMesh);
	}

	onMounted(() => {
		cleanup();
		currentMesh = unref(mesh);

		if (currentMesh) {
			bloomSelection.add(currentMesh);
		}
	});

	if (isRef(mesh)) {
		watch(mesh, () => {
			cleanup();
			currentMesh = unref(mesh);

			if (currentMesh) {
				bloomSelection.add(currentMesh);
			}
		});
	}

	onScopeDispose(cleanup)
}