import { onScopeDispose, ref } from "vue";
import { runBlocking, waitBlocking } from "../utils/runBlocking";

const target = new EventTarget();
export let currentFrame = ref(0);

let lock = false;
async function handleRun() {
	await waitBlocking();
	lock = false;
	run();
}

function run() {
	if (lock) return;
	lock = true;

	// invoke callbacks
	currentFrame.value++;
	target.dispatchEvent(new CustomEvent("render"));

	// request an animation frame and wait for callbacks before next render
	requestAnimationFrame(handleRun);
}

run();

export function useRenderLoop(callback: (currentFrame: number) => any) {
	const handleEvent = () => runBlocking(callback(currentFrame.value));
	target.addEventListener("render", handleEvent);

	onScopeDispose(() => {
		window.removeEventListener("render", handleEvent);
	});
}
